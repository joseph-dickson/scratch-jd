<?php
/**
 *	@since Scratch JD 1.0.0
 *
 * 	Proper way to enqueue scripts and styles
 */

function scratch_jd_scripts() {

	wp_enqueue_style( 'scratch-jd', get_stylesheet_uri() );
}

add_action( 'wp_enqueue_scripts', 'scratch_jd_scripts' );
