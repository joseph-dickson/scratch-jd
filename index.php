<?php
// Get the Header
get_header();

// The Loop
if ( have_posts() ) {
        while ( have_posts() ) {
                the_post(); 

                // Display the post title and HTML              
                the_title();
                
                // Display post content
                the_content();
                
        } // end while
} // end if

// Get the Footer
get_footer();
